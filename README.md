# HTW Berlin Class "Wissensrepräsentation", Sommer Semester 2018
# B. Sc. "Applied Computer Science"


The course "Wissensrepräsentation" provides basic knowledge of **machine learning***. In particular, the following contents were covered:
- [Univariate Linear Regression](https://github.com/dltcls/wissenrepr-sentation_machine-learning_htw/blob/master/Beleg1_Univariate-lineare-Regression.ipynb)
- [Multivariate Linear Regression](https://github.com/dltcls/wissenrepr-sentation_machine-learning_htw/blob/master/Beleg2_MultivariateLineareRegression_FINAL.ipynb)
- [Logistic Regression with Regularization and Bias/Variance Analysis example](https://github.com/dltcls/wissenrepr-sentation_machine-learning_htw/blob/master/Beleg3_logistische_Regression_FINAL.ipynb)
- [Logistic Regression - Decision Trees](https://github.com/dltcls/wissenrepr-sentation_machine-learning_htw/blob/master/Beleg4_decision-trees_Titanic_dataset-FINAL.ipynb)

Programming language: Python
